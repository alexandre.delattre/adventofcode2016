package day5

import utils.HASH


fun solve(s: String) = generateSequence(0) { it + 1 }
        .map { "$s$it" }
        .map { HASH.md5(it) }
        .filter { it.startsWith("00000") }
        .map { it[5] }
        .onEach { print(".") }
        .take(8)
        .joinToString("")


val validPos = (0..7).map { it.toString()[0] }

fun solve2(s: String) = generateSequence(0) { it + 1 }
        .map { "$s$it" }
        .map { HASH.md5(it) }
        .filter { it.startsWith("00000") }
        .map { it[5] to it[6] }
        .filter { it.first in validPos }
        .distinctBy { it.first }
        .take(8)
        .onEach { print(".") }
        .sortedBy { it.first }
        .map { it.second }
        .joinToString("")

fun main(args: Array<String>) {
    println(solve("abc"))
    println(solve("ojvtpuvg"))
    println(solve2("abc"))
    println(solve2("ojvtpuvg"))
}