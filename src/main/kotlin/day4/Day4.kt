package day4

import utils.openResource

data class Room(val name: String, val sectorId: Int, val checksum: String) {
    val isValid: Boolean
        get() = computeChecksum(name) == checksum

    fun decryptName() = name.map { if (it == '-') ' ' else rotate(it, sectorId) }.joinToString("")
}

private val pattern = Regex("""([a-z\-]*)-(\d+)\[([a-z]{5})]""")

fun parse(lines: Sequence<String>) = lines.filter { it.isNotBlank() }.mapNotNull { line ->
    pattern.find(line)?.let {
        val (_, name, sectorId, checksum) = it.groupValues
        Room(name, sectorId.toInt(), checksum)
    }
}

fun computeChecksum(name: String) = name.filter { it != '-' }.fold(emptyMap<Char, Int>()) { freqs, c ->
    freqs + (c to freqs.getOrDefault(c, 0) + 1)
}.toList().sortedWith(compareBy({ -it.second }, { it.first })).map { it.first }.take(5).joinToString("")

fun solve(path: String) {
    val rooms = openResource(path).useLines { parse(it).toList() }
    val validRooms = rooms.filter { it.isValid }
    println(validRooms.sumBy { it.sectorId })
    validRooms.map { it.sectorId to it.decryptName() }.find { (_, name) -> name.contains("northpole")}?.let { (id, name) ->
        println("$id : $name")
    } ?: println("not found")
}

val alphabet = "abcdefghijklmnopqrstuvwxyz"
fun rotate(char: Char, i: Int) = alphabet[(alphabet.indexOf(char) + i) % 26]


fun main(args: Array<String>) {
    solve("day4/Sample.txt")
    solve("day4/Problem.txt")


}