package utils

import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by Alexandre Delattre on 27/08/2017.
 */

fun openResource(path: String) = object {}::class.java.classLoader.getResource(path).openStream().bufferedReader()

fun readResourceLines(path: String): Flowable<String> = Flowable.using(
        { openResource(path) },
        { r -> Flowable.fromIterable(Iterable { r.lines().iterator() }) },
        { r -> r.close() }
)

fun putStrLn(s: String) = Single.fromCallable { println(s) }

@Suppress("RedundantUnitReturnType")
fun runMain(block: () -> Single<*>): Unit = block()
        .flatMap { putStrLn("Result:\n$it") }
        .blockingGet()

