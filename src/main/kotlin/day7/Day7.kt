package day7

import io.reactivex.rxkotlin.Singles
import utils.readResourceLines
import utils.runMain

/**
 * Created by Alexandre Delattre on 22/11/2017.
 */

data class Ipv7(val supernet: List<String>, val hypernet: List<String>)

fun parseLine(line: String) = line.split('[', ']').let {
    Ipv7(it.filterIndexed { i, _ -> i % 2 == 0 }, it.filterIndexed { i, _ -> i % 2 == 1 })
}

fun containsABBA(s: String) = s.windowed(4).any { isABBA(it) }

fun isABBA(s: String) = s[0] != s[1] && s[0] == s[3] && s[1] == s[2]

val Ipv7.supportTLS get() = supernet.any(::containsABBA) && hypernet.none(::containsABBA)

fun allABAs(s: String) = s.windowed(3).filter { isABA(it) }

fun isABA(s: String) = s[0] != s[1] && s[0] == s[2]

val Ipv7.supportSSL
    get() = supernet.flatMap { allABAs(it) }.any { aba ->
        val bab = "" + aba[1] + aba[0] + aba[1]
        hypernet.any { it.contains(bab) }
    }

fun main(args: Array<String>) = runMain {
    readResourceLines("day7/Day7.txt")
            .filter { it.isNotBlank() }
            .map(::parseLine).publish {
        Singles.zip(
                it.filter { it.supportTLS }.count(),
                it.filter { it.supportSSL }.count()
        ).toFlowable()
    }.singleOrError()
}