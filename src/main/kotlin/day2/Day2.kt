package day2

import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import utils.putStrLn
import utils.readResourceLines
import utils.runMain

enum class Move {
    UP, DOWN, LEFT, RIGHT
}

fun parseMove(s: String) = s.map {
    when (it) {
        'U' -> Move.UP
        'D' -> Move.DOWN
        'L' -> Move.LEFT
        'R' -> Move.RIGHT
        else -> throw Exception()
    }
}

data class DigitPosition(val i: Int, val j: Int)

interface Digipad {
    val initialPosition: DigitPosition

    fun applyMove(move: Move, position: DigitPosition): DigitPosition {
        val (i, j) = position
        val newPosition = when (move) {
            Move.UP -> DigitPosition(i - 1, j)
            Move.DOWN -> DigitPosition(i + 1, j)
            Move.LEFT -> DigitPosition(i, j - 1)
            Move.RIGHT -> DigitPosition(i, j + 1)
        }
        return if (getDigit(newPosition) != null) newPosition else position
    }

    fun getDigit(position: DigitPosition): String?
}

abstract class BaseDigipad : Digipad {
    protected abstract val digits: List<List<String?>>
    override fun getDigit(position: DigitPosition) = digits.getOrNull(position.i)?.getOrNull(position.j)
}

class SimpleDigipad : BaseDigipad() {
    override val initialPosition = DigitPosition(1, 1)
    override val digits = listOf(
            listOf("1", "2", "3"),
            listOf("4", "5", "6"),
            listOf("7", "8", "9")
    )

}

class ComplexDigipad : BaseDigipad() {
    override val initialPosition = DigitPosition(2, 0)
    private val NOP = null
    override val digits = listOf(
            listOf(NOP, NOP, "1", NOP, NOP),
            listOf(NOP, "2", "3", "4", NOP),
            listOf("5", "6", "7", "8", "9"),
            listOf(NOP, "A", "B", "C", NOP),
            listOf(NOP, NOP, "D", NOP, NOP)
    )
}

fun solve(data: Flowable<String>, digipad: Digipad): Single<String> = data
        .filter(String::isNotBlank)
        .map(::parseMove)
        .reduce("" to digipad.initialPosition) { (digits, position), moves ->
            val lastPosition = moves.fold(position) { p, m -> digipad.applyMove(m, p) }
            digits + digipad.getDigit(lastPosition) to lastPosition
        }.map { it.first }


private fun solveAndPrint(path: String, digipad: BaseDigipad): Single<Unit> = readResourceLines(path)
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.computation())
        .let { solve(it, digipad) }
        .flatMap { putStrLn("$digipad - $path : $it") }

fun main(args: Array<String>) = runMain {
    val problems = listOf(SimpleDigipad(), ComplexDigipad()).flatMap { digipad ->
        listOf("Sample.txt", "Problem.txt").map { "/day2/$it" }.map { path -> solveAndPrint(path, digipad) }
    }

    Single.zipArray({ it }, problems.toTypedArray())
}

