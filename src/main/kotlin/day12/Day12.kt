package day12

import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import utils.*

enum class Register {
    A, B, C, D
}

sealed class Instruction

data class Cpy(val from: Either<Int, Register>, val to: Register) : Instruction()
data class Inc(val register: Register) : Instruction()
data class Dec(val register: Register) : Instruction()
data class Jnz(val value: Either<Int, Register>, val offset: Int) : Instruction()

private fun parseRegister(reg: String) = when (reg) {
    "a" -> Register.A
    "b" -> Register.B
    "c" -> Register.C
    "d" -> Register.D
    else -> throw Exception("Invalid register $reg")
}

private fun String.literalOrRegister() = toIntOrNull()?.let { Left(it) } ?: Right(parseRegister(this))

fun parseInstruction(line: String): Instruction {
    val parts = line.split(" ")
    val opCode = parts[0]
    return when (opCode) {
        "cpy" -> {
            val (_, x, y) = parts
            Cpy(x.literalOrRegister(), parseRegister(y))
        }
        "inc" -> {
            val (_, x) = parts
            Inc(parseRegister(x))
        }
        "dec" -> {
            val (_, x) = parts
            Dec(parseRegister(x))
        }
        "jnz" -> {
            val (_, x, y) = parts
            Jnz(x.literalOrRegister(), y.toInt())
        }
        else -> throw Exception("Invalid opCode $opCode")
    }
}


data class CpuState(val a: Int, val b: Int, val c: Int, val d: Int, val pc: Int) {
    operator fun get(register: Register) = when (register) {
        Register.A -> a
        Register.B -> b
        Register.C -> c
        Register.D -> d
    }

    fun update(register: Register, value: Int) = when (register) {
        Register.A -> copy(a = value, pc = pc + 1)
        Register.B -> copy(b = value, pc = pc + 1)
        Register.C -> copy(c = value, pc = pc + 1)
        Register.D -> copy(d = value, pc = pc + 1)
    }
}

private fun Either<Int, Register>.value(state: CpuState) = fold({ it }, { state[it] })

fun execute(instructions: List<Instruction>, initialState: CpuState = CpuState(0, 0, 0, 0, 0)): Int {
    tailrec fun aux(state: CpuState): Int {
        val i = instructions.getOrNull(state.pc) ?: return state.a
        val newState = when (i) {
            is Cpy -> state.update(i.to, i.from.value(state))
            is Inc -> state.update(i.register, state[i.register] + 1)
            is Dec -> state.update(i.register, state[i.register] - 1)
            is Jnz -> if (i.value.value(state) != 0) {
                state.copy(pc = state.pc + i.offset)
            } else {
                state.copy(pc = state.pc + 1)
            }
        }
        return aux(newState)
    }
    return aux(initialState)
}

fun main(args: Array<String>) = runMain {
    readResourceLines("day12/Problem.txt")
            .subscribeOn(Schedulers.io())
            .filter(String::isNotBlank)
            .map(::parseInstruction)
            .toList()
            .toObservable().publish { instructions ->

        Observables.zip(
                instructions.observeOn(Schedulers.computation()).map { execute(it) },
                instructions.observeOn(Schedulers.computation()).map { execute(it, CpuState(0, 0, 1, 0, 0)) }
        )
    }.singleOrError()
}