package day6

import io.reactivex.Single
import io.reactivex.rxkotlin.Singles
import utils.openResource
import utils.readResourceLines
import utils.runMain

/**
 * Created by Alexandre Delattre on 09/09/2017.
 */

private fun solve(path: String, f: (Frequencies) -> Char): Single<String> = readResourceLines(path).toList()
        .map { lines ->
            (0 until lines[0].length)
                    .map { i -> lines.map { it[i] } }
                    .map { getFrequencies(it) }
                    .map(f)
                    .joinToString("")
        }

private fun solve1(path: String): Single<String> = solve(path) { it.first().first }

private fun solve2(path: String): Single<String> = solve(path) { it.last().first }

typealias Frequencies = List<Pair<Char, Int>>

private fun getFrequencies(chars: List<Char>): Frequencies = chars.fold(emptyMap<Char, Int>()) { freqs, c ->
    freqs + (c to freqs.getOrDefault(c, 0) + 1)
}.toList().sortedByDescending { it.second }

fun main(args: Array<String>) = runMain {
    Singles.zip(
            solve1("day6/Sample.txt"),
            solve2("day6/Sample.txt"),
            solve1("day6/Input.txt"),
            solve2("day6/Input.txt")
    ) { a, b, c, d -> listOf(a, b, c, d) }
}