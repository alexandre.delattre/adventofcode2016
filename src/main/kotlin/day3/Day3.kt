package day3

import utils.openResource
import java.util.regex.Pattern

typealias TriangleParser = (Sequence<String>) -> Sequence<Triangle>

data class Triangle(val a: Int, val b: Int, val c: Int) {
    fun isValid() = a + b > c && a + c > b && b + c > a
}

private val whitespace = Pattern.compile("\\s+")

private fun String.parseTriangle() = trim().split(whitespace).map { it.toInt() }

val parseTriangles: TriangleParser = { it.map { line ->
    val (a, b, c) = line.parseTriangle()
    Triangle(a, b, c)
}}

val parseColumns: TriangleParser = { it.chunked(3).flatMap { (line1, line2, line3) ->
    val (a1, b1, c1) = line1.parseTriangle()
    val (a2, b2, c2) = line2.parseTriangle()
    val (a3, b3, c3) = line3.parseTriangle()
    sequenceOf(Triangle(a1, a2, a3), Triangle(b1, b2, b3), Triangle(c1, c2, c3))
}}

fun numberOfValidTriangles(dataPath: String, parser: TriangleParser) = openResource(dataPath).useLines {
    parser(it.filter { it.isNotBlank() }).filter(Triangle::isValid).count()
}

fun main(args: Array<String>) {
    println(numberOfValidTriangles("day3/Data.txt", parser = parseTriangles))
    println(numberOfValidTriangles("day3/Data.txt", parser = parseColumns))
}
