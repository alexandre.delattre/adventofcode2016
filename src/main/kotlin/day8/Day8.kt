package day8

import utils.readResourceLines
import utils.runMain

/**
 * Created by Alexandre Delattre on 24/11/2017.
 */

val rectRegex = Regex("""rect (\d+)x(\d+)""")
val rotateRowRegex = Regex("""rotate row y=(\d+) by (\d+)""")
val rotateColumnRegex = Regex("""rotate column x=(\d+) by (\d+)""")

sealed class LcdInstruction
data class DrawRect(val w: Int, val h: Int) : LcdInstruction()
data class RotateRow(val y: Int, val by: Int) : LcdInstruction()
data class RotateCol(val x: Int, val by: Int) : LcdInstruction()

fun parseLine(line: String): LcdInstruction = rectRegex.matchEntire(line)?.let {
    DrawRect(it.groupValues[1].toInt(), it.groupValues[2].toInt())
} ?: rotateRowRegex.matchEntire(line)?.let {
    RotateRow(it.groupValues[1].toInt(), it.groupValues[2].toInt())
} ?: rotateColumnRegex.matchEntire(line)?.let {
    RotateCol(it.groupValues[1].toInt(), it.groupValues[2].toInt())
} ?: error("Invalid line")


data class Lcd(val w: Int = 50, val h: Int = 6, val array: Array<Array<Boolean>> = Array(w) { Array(h) { false } }) {

    private fun copyArray() = Array(w) { i -> Array(h) { j -> array[i][j] }}

    fun execute(instruction: LcdInstruction): Lcd = when (instruction) {
        is DrawRect -> execute(instruction)
        is RotateRow -> execute(instruction)
        is RotateCol -> execute(instruction)
    }

    private fun execute(rect: DrawRect) = copy(array = copyArray().apply {
        for (i in 0 until rect.w) for (j in 0 until rect.h) this[i][j] = true
    })

    private fun execute(r: RotateCol) = copy(array = copyArray().apply {
        for (j in 0 until h) this[r.x][(j + r.by) % h] = array[r.x][j]
    })

    private fun execute(r: RotateRow) = copy(array = copyArray().apply {
        for (i in 0 until w) this[(i + r.by) % w][r.y] = array[i][r.y]
    })

    override fun toString(): String = StringBuilder().apply {
        for (j in 0 until h) {
            for (i in 0 until w) {
                append(if (array[i][j]) '#' else ' ')
            }
            append('\n')
        }
    }.toString()

    val numberLits get() = array.sumBy { it.filter { it }.size }
}

fun main(args: Array<String>) = runMain {
    readResourceLines("day8/Day8.txt")
            .filter { it.isNotBlank() }
            .map(::parseLine)
            .reduce(Lcd(), Lcd::execute)
            .map { "$it\n${it.numberLits}" }
}